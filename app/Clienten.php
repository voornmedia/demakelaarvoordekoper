<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clienten extends Model
{

  protected $table = 'clientens';

  public function taxaties()
  {
    return $this -> hasOne('App\Taxatie');
  }

  protected $fillable = [
       'geslacht', 'titel', 'voorletters', 'voornaam', 'achternaam',
   ];
}
