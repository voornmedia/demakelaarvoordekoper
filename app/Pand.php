<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pand extends Model
{
  //public function panden() {
  //  return $this -> belongsTo('App\Taxatie');
  //}

  protected $table = 'pands';

  public function taxaties()
  {
    return $this -> hasOne('App\Taxatie');
  }

  protected $fillable = [
       'straat', 'postcode', 'plaats', 'makelaar', 'woonoppervlakte',
   ];
}
