<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Taxatie;
use App\Clienten;
use App\Pand;
use Session;
use PDF;
use Image;

class TaxatieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //variable aanmaken waar alle clienten inzitten
      $taxaties = Taxatie::all();
      $clienten = clienten::all();
      $panden = pand::all();

        //return view ('taxaties/taxaties');
        return view("taxaties.taxaties",compact('taxaties','clienten', 'panden'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $clienten = clienten::all();
      $panden = pand::all();
        return view('taxaties.nieuweTaxatie', compact('clienten', 'panden'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //data valideren
      $this->validate($request, array(
        //'titel'         => 'nullable|max:50',
        'pand_id'       => 'required|integer',
        'clienten_id'       => 'required|integer',
        //'rapport'       => 'required|max:10000',
        'soort_woning'     => 'required|max:50',
        'type_woning'     => 'required|max:50',
        //'woning_omschrijving'     => 'required|integer',
        'hyp4'     => 'required|integer',

      ));

      //$rapport = 'dit is het rapport van de '.$request->pand['straat'];

      // in database zetten
      $taxatie = new Taxatie;

      //$taxatie->titel = $request->titel;
      $taxatie->pand_id = $request->pand_id;
      //$taxatie->rapport = $rapport;
      $taxatie->clienten_id = $request->clienten_id;
      $taxatie->soort_woning = $request->soort_woning;
      $taxatie->type_woning = $request->type_woning;
      $taxatie->hyp4 = $request->hyp4;
      $myCheckboxes = $request->input('omschrijving');
      $exampleEncoded = json_encode($myCheckboxes);
      $taxatie->woning_omschrijving = $exampleEncoded;

      if ($request->hasFile('featured_image')) {
        $image = $request->file('featured_image');
        $filename = time() . '.' . $image->getClientOriginalExtension();
        $location = public_path('img/taxaties/' . $filename);
        Image::make($image)->resize(600, 400)->save($location);

        $taxatie->afbeelding = $filename;
      }

      $taxatie->save();
      //melding als succesvol
      Session::flash('succes', 'Taxatierapport succesvol aangemaakt');

      // naar volgende pagina sturen
      return redirect()->route('taxatie.show', $taxatie->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $taxatie = Taxatie::find($id);
      return view('taxaties.showTaxatie')->with('taxatie', $taxatie);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //find the post in the databse and save as variable
        $taxatie = Taxatie::find($id);
        //zoek op id de rij van andere tabellen
        $clienten = clienten::all();
        $clients = array();
        foreach ( $clienten as $client){
          $clients[$client->id] = $client->voornaam;
        }

        $panden = pand::all();
        $pands = array();
        foreach ( $panden as $pand){
          $pands[$pand->id] = $pand->straat;
        }
        //return the view and pass in varible
        return view('taxaties.wijzigTaxatie', compact('taxatie','clients', 'pands'));

        //return view('taxaties.wijzigTaxatie')->with('taxatie', $taxatie);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //valideer de data
        $this->validate($request, array (
          'pand_id'       => 'required|integer',
          'clienten_id'     => 'required|integer',
          'soort_woning'     => 'required|max:50',
          'type_woning'     => 'required|max:50',
          'hyp4'     => 'required|integer',

        ));
        //save de data
        $taxatie = Taxatie::find($id);

        $taxatie->pand_id = $request->input('pand_id');
        $taxatie->clienten_id = $request->input('clienten_id');
        $taxatie->soort_woning = $request->input('soort_woning');
        $taxatie->type_woning = $request->input('type_woning');
        $taxatie->hyp4 = $request->input('hyp4');

        $taxatie->save();
        //succesbericht flash
        Session::flash('succes', 'Taxatierapport succesvol aangepast.');

        //redirect met flash naar view taxatie.show
        return redirect()->route('taxatie.show', $taxatie->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $taxatie = Taxatie::find($id);

        $taxatie->delete();

        Session::flash('succes', 'Taxatie succesvol verwijderd!');
        return redirect()->route('taxatie.index');
    }

    public function pdf($id) {
      $taxatie = Taxatie::find($id);
      $straat = $taxatie->pand['straat'];
      $plaats = $taxatie->pand['plaats'];
      $nr = $taxatie->pand['huisnummer'];
      $naam = "Taxatierapport ".$straat." ".$nr." ".$plaats.".pdf";

      $pdf = PDF::loadView('taxaties.rapport', compact('taxatie'));
      return $pdf->stream($naam);
    }
}
