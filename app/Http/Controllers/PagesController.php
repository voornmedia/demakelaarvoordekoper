<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Clienten;
use App\Pand;
use App\Taxatie;
use Mail;
use Session;

Class PagesController extends Controller {

  public function getIndex () {
    #procces bepaalkde variable data
    # praat met model
    # ontvang van model
    # proces data van model
    # pass to correct view

    $clienten = Clienten::orderBy('created_at', 'desc')->limit(5)->get();
    $panden = Pand::orderBy('created_at', 'desc')->limit(5)->get();
    $taxaties = Taxatie::orderBy('created_at', 'desc')->limit(5)->get();

    return view("pages/welcome",compact('clienten','panden', 'taxaties'));
  }

  public function getNieuweClient () {
    return view ('pages/nieuweClient');
  }

  public function getNieuwePand () {
    return view ('panden/nieuwPand');
  }

  public function getInloggen () {
    return view ('pages/inloggen');
  }

  public function getRegister () {
    return view ('login/register');
  }

  public function getLogin () {
    return view ('login/login');
  }
  public function getContact () {
    return view ('pages/contact');
  }

  public function postContact(Request $request) {
		$this->validate($request, [
			'email' => 'required|email',
			'subject' => 'min:3',
			'message' => 'min:10']);

		$data = array(
			'email' => $request->email,
			'subject' => $request->subject,
			'bericht' => $request->message
			);

		Mail::send('emails.contact', $data, function($message) use ($data){
      $message->from($data['email']);
      $message->to('info@voornmedia.nl');
      $message->subject($data['subject']);
		});

		Session::flash('success', 'Your Email was Sent!');

		return redirect('/');
	}

}
