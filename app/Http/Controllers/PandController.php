<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Pand;
use Session;
use Excel;

class PandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //variable aanmaken waar alle clienten inzitten
      $panden = Pand::all();

      //zend de bovenstaande variable naar de juiste view
      return view("panden.panden",compact('panden'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panden.nieuwPand');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //data valideren
      $this->validate($request, array(
        'straat'=> 'nullable|max:50',
        'huisnummer'=> 'nullable|max:10',
        'toevoeging'=> 'nullable|max:10',
        'postcode'=> 'required|max:10',
        'plaats'=> 'nullable|max:50',
        'huidigePrijs'=> 'nullable|max:10',
        'koopConditie'=> 'nullable|max:50',
        'bouwjaar'=> 'nullable|max:10',
      ));
      // in database zetten
      $pand = new Pand;

      $pand->straat = $request->straat;
      $pand->huisnummer = $request->huisnummer;
      $pand->toevoeging = $request->toevoeging;
      $pand->postcode = $request->postcode;
      $pand->plaats = $request->plaats;
      $pand->huidigePrijs = $request->huidigePrijs;
      $pand->koopConditie = $request->koopConditie;
      $pand->bouwjaar = $request->bouwjaar;

      $pand->save();
      //melding als succesvol
      Session::flash('succes', 'Pand succesvol toegevoegd!');

      // naar volgende pagina sturen
      return redirect()->route('pand.show', $pand->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $pand = Pand::find($id);
      return view('panden.wijzigPand')->with('pand', $pand);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $pand = Pand::find($id);
      return view('panden.wijzigPand')->with('pand', $pand);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      //valideer de data
      $this->validate($request, array (
        'straat'=> 'required|max:50',
        'huisnummer'=> 'required|max:15',
        'toevoeging'=> 'nullable|max:10',
        'postcode'=> 'required|max:25',
        'plaats'=> 'required|max:10',
        'makelaar'=> 'nullable|max:50',
        'telMakelaar'=> 'nullable|max:50',
        'aanmeldsoort'=> 'nullable|max:10',
        'huidigePrijs'=> 'nullable|max:50',
        'koopConditie'=> 'nullable|max:50',
        'prijsPerM2'=> 'nullable|max:50',
        'transactieprijs'=> 'nullable|max:50',

        'transactieprijsPerM2'=> 'nullable|max:50',
        'huidigeHuur'=> 'nullable|max:10',
        'huurConditie'=> 'nullable|max:50',
        'vertrouwelijk'=> 'nullable|max:50',
        'datumAanmelding'=> 'nullable|max:50',
        'voorbehoudVanaf'=> 'nullable|max:50',
        'voorbehoudTot'=> 'nullable|max:50',
        'datumAfmelding'=> 'nullable|max:10',
        'ondertekeningAkte'=> 'nullable|max:50',
        'transactiedatum'=> 'nullable|max:50',
        'transportdatum'=> 'nullable|max:50',
        'ontbindendeVoorwaardenFin'=> 'nullable|max:50',
        'waarborg'=> 'nullable|max:50',
        'dagenOpDeMarkt'=> 'nullable|max:10',
        'woonoppervlakte'=> 'nullable|max:50',
        'inhoudWoning'=> 'nullable|max:50',
        'perceelOppervlak'=> 'nullable|max:50',
        'overigeInpandigeRuimte'=> 'nullable|max:50',
        'externeBergruimte'=> 'nullable|max:50',
        'gebouwGebBuitenruimte'=> 'nullable|max:10',
        'woonkamer'=> 'nullable|max:50',
        'bouwjaar'=> 'nullable|max:50',
        'aantalKamers'=> 'nullable|max:50',
        'soortOG'=> 'nullable|max:50',

        'soortWoning'=> 'nullable|max:50',
        'soortAppartement'=> 'nullable|max:10',
        'typeWoning'=> 'nullable|max:50',
        'kwaliteitWoning'=> 'nullable|max:50',
        'subtypeWoning'=> 'nullable|max:50',
        'energielabel'=> 'nullable|max:50',
        'energieIndex'=> 'nullable|max:50',
        'energieEindDatum'=> 'nullable|max:10',
        'status'=> 'nullable|max:50',
        'medeaanmelder'=> 'nullable|max:50',
        'prijsAanvaarding'=> 'nullable|max:50',
        'aantalPrijswijzigingen'=> 'nullable|max:50',

        'aangemeldVoor'=> 'nullable|max:50',
        'prijswijziging1'=> 'nullable|max:10',
        'datum1'=> 'nullable|max:50',
        'verschil1'=> 'nullable|max:50',
        'prijswijziging2'=> 'nullable|max:10',
        'datum2'=> 'nullable|max:50',
        'verschil2'=> 'nullable|max:50',
        'prijswijziging3'=> 'nullable|max:10',
        'datum3'=> 'nullable|max:50',
        'verschil3'=> 'nullable|max:50',
        'prijswijziging4'=> 'nullable|max:10',
        'datum4'=> 'nullable|max:50',
        'verschil4'=> 'nullable|max:50',
        'prijswijziging5'=> 'nullable|max:10',
        'datum5'=> 'nullable|max:50',
        'verschil5'=> 'nullable|max:50',

      ));
      // in database zetten
      $pand = Pand::find($id);

      $pand->straat = $request->straat;
      $pand->huisnummer = $request->huisnummer;
      $pand->toevoeging = $request->toevoeging;
      $pand->postcode = $request->postcode;
      $pand->plaats = $request->plaats;
      $pand->makelaar = $request->makelaar;
      $pand->telMakelaar = $request->telMakelaar;
      $pand->aanmeldsoort = $request->aanmeldsoort;
      $pand->huidigePrijs = $request->huidigePrijs;
      $pand->koopConditie = $request->koopConditie;
      $pand->prijsPerM2 = $request->prijsPerM2;
      $pand->transactieprijs = $request->transactieprijs;
      $pand->transactieprijsPerM2 = $request->transactieprijsPerM2;
      $pand->huidigeHuur = $request->huidigeHuur;
      $pand->huurConditie = $request->huurConditie;
      $pand->vertrouwelijk = $request->vertrouwelijk;
      $pand->datumAanmelding = $request->datumAanmelding;
      $pand->voorbehoudVanaf = $request->voorbehoudVanaf;
      $pand->voorbehoudTot = $request->voorbehoudTot;
      $pand->datumAfmelding = $request->datumAfmelding;
      $pand->ondertekeningAkte = $request->ondertekeningAkte;
      $pand->transactiedatum = $request->transactiedatum;
      $pand->transportdatum = $request->transportdatum;
      $pand->ontbindendeVoorwaardenFin = $request->ontbindendeVoorwaardenFin;
      $pand->waarborg = $request->waarborg;
      $pand->dagenOpDeMarkt = $request->dagenOpDeMarkt;
      $pand->woonoppervlakte = $request->woonoppervlakte;
      $pand->inhoudWoning = $request->inhoudWoning;
      $pand->perceelOppervlak = $request->perceelOppervlak;
      $pand->overigeInpandigeRuimte = $request->overigeInpandigeRuimte;
      $pand->externeBergruimte = $request->externeBergruimte;
      $pand->gebouwGebBuitenruimte = $request->gebouwGebBuitenruimte;
      $pand->woonkamer = $request->woonkamer;
      $pand->bouwjaar = $request->bouwjaar;
      $pand->aantalKamers = $request->aantalKamers;
      $pand->soortOG = $request->soortOG;
      $pand->soortWoning = $request->soortWoning;
      $pand->soortAppartement = $request->soortAppartement;
      $pand->typeWoning = $request->typeWoning;
      $pand->kwaliteitWoning = $request->kwaliteitWoning;
      $pand->subtypeWoning = $request->subtypeWoning;
      $pand->energielabel = $request->energielabel;
      $pand->energieIndex = $request->energieIndex;
      $pand->energieEindDatum = $request->energieEindDatum;
      $pand->status = $request->status;
      $pand->medeaanmelder = $request->medeaanmelder;
      $pand->prijsAanvaarding = $request->prijsAanvaarding;
      $pand->aantalPrijswijzigingen = $request->aantalPrijswijzigingen;
      $pand->aangemeldVoor = $request->aangemeldVoor;
      $pand->prijswijziging1 = $request->prijswijziging1;
      $pand->datum1 = $request->datum1;
      $pand->verschil1 = $request->verschil1;
      $pand->prijswijziging2 = $request->prijswijziging2;
      $pand->datum2 = $request->datum2;
      $pand->verschil2 = $request->verschil2;
      $pand->prijswijziging3 = $request->prijswijziging3;
      $pand->datum3 = $request->datum3;
      $pand->verschil3 = $request->verschil3;
      $pand->prijswijziging4 = $request->prijswijziging4;
      $pand->datum4 = $request->datum4;
      $pand->verschil4 = $request->verschil4;
      $pand->prijswijziging5 = $request->prijswijziging5;
      $pand->datum5 = $request->datum5;
      $pand->verschil5 = $request->verschil5;


      $pand->save();
      //melding als succesvol
      Session::flash('succes', 'pand succesvol gewijzigd!');

      //redirect met flash naar view taxatie.show
      return redirect()->route('pand.index', $pand->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $pand = Pand::find($id);

        $pand->delete();

        Session::flash('succes', 'Pand succesvol verwijderd!');
        return redirect()->route('pand.index');
    }


    /**

 * Import file into database Code

 *

 * @var array

 */

public function pandImportNew(Request $request){
    if($request->hasFile('file')){
        $path = $request->file('file')->getRealPath();
        $data = \Excel::load($path)->get();
        if(!empty($data) && $data->count()){
            foreach ($data->toArray() as $key => $value) {
                if(!empty($value)){
                    foreach ($value as $v) {
                        $insert[] = [
                          'straat' => $v['straat'],
                          'huisnummer' => $v['huisnr.'],
                          'toevoeging' => $v['toev.'],
                          'postcode' => $v['postcode'],
                          'plaats' => $v['plaats'],
                          'makelaar' => $v['makelaar'],
                          //'woonoppervlakte' => $v['woonoppervlakte']

                          'telMakelaar' => $v['tel_makelaar'],
                          'aanmeldsoort' => $v['aanmeldingsoort'],
                          'huidigePrijs' => $v['huidige_prijs'],
                          'koopConditie' => $v['koop_conditie'],
                          'prijsPerM2' => $v['prijs_per_m2'],
                          //'transactieprijs' => $v['transactie_prijs_/_huur'],
                          'transactieprijsPerM2' => $v['transactieprijs_per_m2'],
                          'huidigeHuur' => $v['huidige_huur'],
                          'huurConditie' => $v['huur_conditie'],
                          'vertrouwelijk' => $v['vertrouwelijk'],
                          'datumAanmelding' => date("Y-m-d H:i:s", strtotime($v['datum_aanmelding'])),
                          'voorbehoudVanaf' => date("Y-m-d H:i:s", strtotime($v['voorbehoud_vanaf'])),
                          'voorbehoudTot' => date("Y-m-d H:i:s", strtotime($v['voorbehoud_tot'])),
                          'datumAfmelding' => date("Y-m-d H:i:s", strtotime($v['datum_afmelding'])),
                          'ondertekeningAkte' => date("Y-m-d H:i:s", strtotime($v['ondertekening_akte'])),
                          'transactiedatum' => date("Y-m-d H:i:s", strtotime($v['transactiedatum'])),
                          'transportdatum' => date("Y-m-d H:i:s", strtotime($v['transportdatum'])),
                          'ontbindendeVoorwaardenFin' => $v['ontb._voorwaarden_fin'],
                          'waarborg' => $v['waarborg'],
                          'dagenOpDeMarkt' => $v['dagen_op_de_markt'],
                          'woonoppervlakte' => $v['woonoppervlakte'],
                          'inhoudWoning' => $v['inhoud_woning'],
                          'perceelOppervlak' => $v['perceel_oppervlak'],
                          'overigeInpandigeRuimte' => $v['overige_inpandige_ruimte'],
                          'externeBergruimte' => $v['externe_bergruimte'],
                          'gebouwGebBuitenruimte' => $v['gebouwgeb._buitenruimte'],
                          'woonkamer' => $v['woonkamer'],
                          //'bouwjaar' => $v['bouwjaar_/_periode'],
                          'aantalKamers' => $v['aantal_kamers'],
                          'soortOG' => $v['soort_og'],
                          'soortWoning' => $v['soort_woning'],
                          'soortAppartement' => $v['soort_appartement'],
                          'typeWoning' => $v['type_woning'],
                          'kwaliteitWoning' => $v['kwaliteit_woning'],
                          //'subtypeWoning' => $v['subtype_woning/appartement'],
                          //'energielabel' => $v['(voorlopig)_energielabel)'],
                          'energieIndex' => $v['energie_index'],
                          'energieEindDatum' => date("Y-m-d H:i:s", strtotime($v['energie_einddatum'])),
                          'status' => $v['status'],
                          'medeaanmelder' => $v['medeaanmelders'],
                          'prijsAanvaarding' => $v['prijs_aanvaarding'],
                          'aantalPrijswijzigingen' => $v['aantal_prijswijzigingen'],
                          'aangemeldVoor' => $v['aangemeld_voor'],
                          'prijswijziging1' => $v['prijswijziging_1'],

                          'prijswijziging2' => $v['prijswijziging_2'],
                          'prijswijziging3' => $v['prijswijziging_3'],
                          'prijswijziging4' => $v['prijswijziging_4'],
                          'prijswijziging5' => $v['prijswijziging_5']
                        ];
                    }
                }
            }
            if(!empty($insert)){
                Pand::insert($insert);
                Session::flash('succes', 'Pand succesvol toegevoegd!');
                return redirect()->route('pand.index');
            }
        }
    }
    return back()->with('error','Please Check your file, Something is wrong there.');

}








}
