<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Clienten;
use Session;
use Excel;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //variable aanmaken waar alle clienten inzitten
        $clienten = Clienten::all();

        //zend de bovenstaande variable naar de juiste view
        return view("pages.clienten",compact('clienten'));
        //return view('pages.clienten')->withClienten($clienten);
        //return view('pages.clienten')->with('clienten', $clienten);
        //return View::make('pages.clienten')->with(compact('Clienten'));
        //return view('pages.wijzigClient')->with('client', $client);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.nieuweClient');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //data valideren
        $this->validate($request, array(
          'geslacht'=> 'nullable|max:10',
          'titel'=> 'nullable|max:15',
          'voorletters'=> 'nullable|max:10',
          'voornaam'=> 'required|max:25',
          'tussenvoegsels'=> 'nullable|max:10',
          'achternaam'=> 'required|max:25',
          'adress'=> 'required|max:50',
          'huisnummer'=> 'required|max:10',
          'woonplaats'=> 'required|max:20',
          'email'=> 'nullable|max:50',
          'telefoon'=> 'nullable|max:20',
          'mobiel'=> 'nullable|max:20',
        ));
        // in database zetten
        $client = new Clienten;

        $client->geslacht = $request->geslacht;
        $client->titel = $request->titel;
        //voorletters was bij orginele migration voortellers, mocht eroor verorzaken terugzetten
        $client->voorletters = $request->voorletters;
        $client->voornaam = $request->voornaam;
        $client->tussenvoegsels = $request->tussenvoegsels;
        $client->achternaam = $request->achternaam;
        $client->adress = $request->adress;
        $client->huisnummer = $request->huisnummer;
        $client->woonplaats = $request->woonplaats;
        $client->email = $request->email;
        $client->telefoon = $request->telefoon;
        $client->mobiel = $request->mobiel;

        $client->save();
        //melding als succesvol
        Session::flash('succes', 'Client succesvol toegevoegd!');

        // naar volgende pagina sturen
        return redirect()->route('client.edit', $client->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $client = Clienten::find($id);
        return view('pages.wijzigClient')->with('client', $client);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $client = Clienten::find($id);
      return view('pages.wijzigClient')->with('client', $client);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      //valideer de data
      $this->validate($request, array (
        'geslacht'=> 'nullable|max:10',
        'titel'=> 'nullable|max:15',
        'voorletters'=> 'nullable|max:10',
        'voornaam'=> 'required|max:25',
        'tussenvoegsels'=> 'nullable|max:10',
        'achternaam'=> 'required|max:25',
        'adress'=> 'required|max:50',
        'huisnummer'=> 'required|max:10',
        'woonplaats'=> 'required|max:20',
        'email'=> 'nullable|max:50',
        'telefoon'=> 'nullable|max:20',
        'mobiel'=> 'nullable|max:20',
      ));
      // in database zetten
      $client = Clienten::find($id);

      $client->geslacht = $request->geslacht;
      $client->titel = $request->titel;
      //voorletters was bij orginele migration voortellers, mocht eroor verorzaken terugzetten
      $client->voorletters = $request->voorletters;
      $client->voornaam = $request->voornaam;
      $client->tussenvoegsels = $request->tussenvoegsels;
      $client->achternaam = $request->achternaam;
      $client->adress = $request->adress;
      $client->huisnummer = $request->huisnummer;
      $client->woonplaats = $request->woonplaats;
      $client->email = $request->email;
      $client->telefoon = $request->telefoon;
      $client->mobiel = $request->mobiel;

      $client->save();
      //melding als succesvol
      Session::flash('succes', 'Client succesvol toegevoegd!');

      //redirect met flash naar view taxatie.show
      return redirect()->route('client.index', $client->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $client = Clienten::find($id);

        $client->delete();

        Session::flash('succes', 'Client succesvol verwijderd!');
        return redirect()->route('client.index');
    }


  /**

* Import file into database Code

*

* @var array

*/

public function clientImportNewClient(Request $request){
  if($request->hasFile('file')){
      $path = $request->file('file')->getRealPath();
      $data = \Excel::load($path)->get();
      if(!empty($data) && $data->count()){
          foreach ($data->toArray() as $key => $value) {
              if(!empty($value)){
                  foreach ($value as $v) {
                      $insert[] = [
                        'geslacht' => $v['geslacht'],
                        'titel' => $v['titel'],
                        'voorletters' => $v['voorletters'],
                        'voornaam' => $v['voornaam'],
                        'tussenvoegsels' => $v['tussenvoegsel'],
                        'achternaam' => $v['achternaam'],
                        'adress' => $v['adress'],
                        'huisnummer' => $v['huisnummer'],
                        'woonplaats' => $v['woonplaats'],
                        'email' => $v['email'],
                        'telefoon' => $v['telefoon'],
                        'mobiel' => $v['mobiel'],
                      ];
                  }
              }
          }
          if(!empty($insert)){
              Clienten::insert($insert);
              Session::flash('succes', 'Pand succesvol toegevoegd!');
              return redirect()->route('client.index');
          }
      }
  }
  return back()->with('error','Please Check your file, Something is wrong there.');

}











}
