<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pands', function (Blueprint $table) {
            $table->increments('id');
            $table->string('straat', 50);
            $table->string('huisnummer', 10)->nullable();
            $table->string('toevoeging', 10)->nullable();
            $table->string('postcode', 10);
            $table->string('plaats', 50);
            $table->string('makelaar', 50)->nullable();
            $table->string('telMakelaar', 50)->nullable();
            $table->string('aanmeldSoort', 50)->nullable();
            $table->decimal('huidigePrijs', 10, 2)->nullable();
            $table->string('koopConditie', 50)->nullable();
            $table->decimal('prijsPerM2', 10, 2)->nullable();
            $table->decimal('transactieprijs', 10, 2)->nullable();
            $table->decimal('transactieprijsPerM2', 10, 2)->nullable();
            $table->decimal('huidigeHuur', 10, 2)->nullable();
            $table->string('huurConditie', 50)->nullable();
            $table->string('vertrouwelijk', 50)->nullable();
            $table->date('datumAanmelding')->nullable();
            $table->date('voorbehoudVanaf')->nullable();
            $table->date('voorbehoudTot')->nullable();
            $table->date('datumAfmelding')->nullable();
            $table->date('ondertekeningAkte')->nullable();
            $table->date('transactiedatum')->nullable();
            $table->date('transportdatum')->nullable();
            $table->date('ontbindendeVoorwaardenFin')->nullable();
            $table->string('waarborg', 50)->nullable();
            $table->integer('dagenOpDeMarkt')->nullable();
            $table->integer('woonoppervlakte')->nullable();
            $table->integer('inhoudWoning')->nullable();
            $table->integer('perceelOppervlak')->nullable();
            $table->integer('overigeInpandigeRuimte')->nullable();
            $table->integer('externeBergruimte')->nullable();
            $table->integer('gebouwGebBuitenruimte')->nullable();
            $table->integer('woonkamer')->nullable();
            $table->integer('bouwjaar')->nullable();
            $table->integer('aantalKamers')->nullable();
            $table->string('soortOG', 50)->nullable();
            $table->string('soortWoning', 50)->nullable();
            $table->string('soortAppartement', 50)->nullable();
            $table->string('typeWoning', 50)->nullable();
            $table->string('kwaliteitWoning', 50)->nullable();
            $table->string('subtypeWoning', 50)->nullable();
            $table->string('energielabel', 50)->nullable();
            $table->string('energieIndex', 50)->nullable();
            $table->date('energieEindDatum')->nullable();
            $table->string('status', 50)->nullable();
            $table->string('medeaanmelder', 50)->nullable();
            $table->string('prijsAanvaarding', 50)->nullable();
            $table->integer('aantalPrijswijzigingen')->nullable();
            $table->decimal('aangemeldVoor', 10, 2)->nullable();
            $table->decimal('prijswijziging1', 10, 2)->nullable();
            $table->date('datum1')->nullable();
            $table->decimal('verschil1', 10, 2)->nullable();
            $table->decimal('prijswijziging2', 10, 2)->nullable();
            $table->date('datum2')->nullable();
            $table->decimal('verschil2', 10, 2)->nullable();
            $table->decimal('prijswijziging3', 10, 2)->nullable();
            $table->date('datum3')->nullable();
            $table->decimal('verschil3', 10, 2)->nullable();
            $table->decimal('prijswijziging4', 10, 2)->nullable();
            $table->date('datum4')->nullable();
            $table->decimal('verschil4', 10, 2)->nullable();
            $table->decimal('prijswijziging5', 10, 2)->nullable();
            $table->date('datum5')->nullable();
            $table->decimal('verschil5', 10, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pands');
    }
}
