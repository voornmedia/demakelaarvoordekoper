<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKadasterToTaxaties extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('taxaties', function (Blueprint $table) {
            //
            $table->string('gemeente')->nullable()->after('hyp4');
            $table->string('sector')->nullable()->after('gemeente');
            $table->string('nummerAindex')->nullable()->after('sector');
            $table->string('grootte_in_m')->nullable()->after('nummerAindex');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('taxaties', function (Blueprint $table) {
          $table->dropColumn('gemeente');
          $table->dropColumn('sector');
          $table->dropColumn('nummerAindex');
          $table->dropColumn('grootte_in_m');
        });
    }
}
