<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVeldenToTaxatie extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('taxaties', function (Blueprint $table) {
          $table->string('soort_woning')->nullable()->after('afbeelding');
          $table->string('type_woning')->nullable()->after('soort_woning');
          $table->string('woning_omschrijving')->nullable()->after('type_woning');
          $table->string('hyp4')->nullable()->after('woning_omschrijving');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('taxaties', function (Blueprint $table) {
            $table->dropColumn('soort_woning');
            $table->dropColumn('type_woning');
            $table->dropColumn('woning_omschrijving');
            $table->dropColumn('hyp4');
        });
    }
}
