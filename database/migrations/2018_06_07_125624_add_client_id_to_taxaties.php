<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddClientIdToTaxaties extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('taxaties', function (Blueprint $table) {
            $table->integer('client_id')->nullable()->after('rapport')->unsigned();
            $table->integer('pand_id')->nullable()->after('titel')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('taxaties', function (Blueprint $table) {
            $table->dropColumn('clienten_id');
            $table->dropColumn('pand_id');
        });
    }
}
