<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clienten', function (Blueprint $table) {
            $table->increments('id');
            $table->string('geslacht', 10)->nullable();
            $table->string('titel', 25)->nullable();
            $table->string('voorletters', 10)->nullable();
            $table->string('voornaam', 25);
            $table->string('tussenvoegsels', 25)->nullable();
            $table->string('achternaam', 25);
            $table->string('adress', 50);
            $table->string('huisnummer', 10);
            $table->string('woonplaats', 20);
            $table->string('email', 50)->nullable();
            $table->string('telefoon', 20)->nullable();
            $table->string('mobiel', 20)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clienten');
    }
}
