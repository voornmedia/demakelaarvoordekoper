<nav class="navbar navbar-default navbar-static-top">
  <!-- Branding Image -->
  <a class="navbar-brand" href="{{ url('/') }}">
      <img src="{{ asset('img/logowhite.png') }}" height="60px" width="180px">
  </a>
    <div class="container">
@guest
    @else
              <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                           <img src="{{ asset('img/icons/icon.gebruikers.png') }}" height="20px" width="20px">      {{ Auth::user()->name }} <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a class="dropdown-item" href="{{ route('logout') }}"
                             onclick="event.preventDefault();
                                           document.getElementById('logout-form').submit();">
                              {{ __('Logout') }}
                          </a>
                          <form id="logout-form" action="{{ route('logout') }}" method="POST" >
                              @csrf
                          </form></li>
                        </ul>
                    </li>
            </ul>
            @endguest
        </div>
</nav>
