@extends('layouts.sideNav')
@section('pageName', 'Overzicht')
@section('titel', 'Overzicht')
@section('content')
<div class="row">
  <div class="col-md-12">
    <h1>Welkom terug</h1>
      <p>Hier komt het welkoms overzicht</p>
  </div>
</div>

<div class="row">

  <div class="col-md-4">
    Laatste 5 clienten
    <div class="table-overzicht">
      <table id="table" class="table table-hover table-bordered table-striped">
        <thead>
          <tr>
            <th>Voornaam</th>
            <th>achternaam</th>
            <th>Mobiel</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($clienten as $client)
            <tr class='clickable-row' data-href='{{ route('client.edit', ['id' => $client->id]) }}'>
              <td> {{ $client ->voornaam}}</td>
              <td> {{ $client ->achternaam}}</td>
              <td> {{ $client ->mobiel}}</td>
            </tr>
        @endforeach
        </tbody>
      </table>

      @push('styles')
        .clickable-row {
          cursor: pointer;
        }
      @endpush
      @push('scripts')
        <script>
        $(document).ready(function() {
          $(".clickable-row").click(function() {
            window.location = $(this).data("href");
          });
        });
        </script>
      @endpush
    </div>
  </div>

  <div class="col-md-4">
    Laatste 5 Panden
    <div class="table-overzicht">
      <table id="table" class="table table-hover table-bordered table-striped">
        <thead>
          <tr>
            <th>Straat</th>
            <th>Postcode</th>
            <th>Plaats</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($panden as $pand)
            <tr class='clickable-row' data-href='{{ route('pand.edit', ['id' => $pand->id]) }}'>
              <td> {{ $pand ->straat}}</td>
              <td> {{ $pand ->postcode}}</td>
              <td> {{ $pand ->plaats}}</td>
            </tr>
        @endforeach
        </tbody>
      </table>

      @push('styles')
        .clickable-row {
          cursor: pointer;
        }
      @endpush
      @push('scripts')
        <script>
        $(document).ready(function() {
          $(".clickable-row").click(function() {
            window.location = $(this).data("href");
          });
        });
        </script>
      @endpush
    </div>
  </div>

  <div class="col-md-4">
    Laatste 5 taxaties
    <div class="table-overzicht">
      <table id="table" class="table table-hover table-bordered table-striped">
        <thead>
          <tr>
            <th>Object</th>
            <th>Aangemaakt</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($taxaties as $taxatie)
            <tr class='clickable-row' data-href='{{ route('taxatie.show', ['id' => $taxatie->id]) }}'>
              <td> {{ $taxatie->pand['straat'] }} {{$taxatie->pand['huisnummer']}}</td>
              <td> {{ date('j M Y', strtotime($taxatie->created_at)) }}</td>
            </tr>
        @endforeach
        </tbody>
      </table>

      @push('styles')
        .clickable-row {
          cursor: pointer;
        }
      @endpush
      @push('scripts')
        <script>
        $(document).ready(function() {
          $(".clickable-row").click(function() {
            window.location = $(this).data("href");
          });
        });
        </script>
      @endpush
    </div>
  </div>

</div>
@endsection
