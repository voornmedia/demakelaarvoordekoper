@extends('layouts.sideNav')

@section('pageName', 'Client toevoegen')

@section('titel', 'Client toevoegen')

@section('content')
<div class="row">
  <div class="col-md-6">
    <h1>Nieuwe Client toevoegen</h1>
      <p>hier komt een form</p>

      {!! Form::open(array('route' => 'client.store', 'data-parsley-validate' => '')) !!}
        {{form::label('geslacht', 'Geslacht:')}}
        {{form::select('geslacht', array('Man' => 'Man', 'Vrouw' => 'Vrouw'))}}
        <br>
        {{form::label('titel', 'Titel:')}}
        {{form::select('titel', array('de heer' => 'de heer', 'mevrouw' => 'mevrouw'))}}
        <br>

        {{form::label('voorletters', 'Voorletters:')}}
        {{form::text('voorletters', null, array('class' => 'form-control'))}}

        {{form::label('voornaam', 'Voornaam:')}}
        {{form::text('voornaam', null, array('class' => 'form-control', 'required' =>'', 'minlength' =>'2', 'maxlength' =>'25'))}}

        {{form::label('tussenvoegsels', 'Tussenvoegsels:')}}
        {{form::text('tussenvoegsels', null, array('class' => 'form-control'))}}

        {{form::label('achternaam', 'Achternaam:')}}
        {{form::text('achternaam', null, array('class' => 'form-control', 'required' =>'', 'minlength' =>'2', 'maxlength' =>'25'))}}

        {{form::label('adress', 'Adress:')}}
        {{form::text('adress', null, array('class' => 'form-control', 'required' =>'', 'minlength' =>'4', 'maxlength' =>'25'))}}

        {{form::label('huisnummer', 'Huisnummer:')}}
        {{form::text('huisnummer', null, array('class' => 'form-control', 'required' =>'', 'minlength' =>'1', 'maxlength' =>'10'))}}

        {{form::label('woonplaats', 'Woonplaats:')}}
        {{form::text('woonplaats', null, array('class' => 'form-control', 'required' =>'', 'minlength' =>'2', 'maxlength' =>'20'))}}

        {{form::label('email', 'Email:')}}
        {{form::text('email', null, array('class' => 'form-control'))}}

        {{form::label('telefoon', 'Telefoon:')}}
        {{form::text('telefoon', null, array('class' => 'form-control'))}}

        {{form::label('mobiel', 'Mobiel:')}}
        {{form::text('mobiel', null, array('class' => 'form-control'))}}

        {{form:: submit('Client toevoegen', array('class' => 'btn btn-succes btn-lg', 'style' => 'margin-top: 10px;'))}}
      {!! Form::close() !!}

  </div>
  <div class="col-md-6">
    {!! Form::open(array('route' => 'client.importNewClient','method'=>'POST','files'=>'true')) !!}
        <div class="row">
           <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    {!! Form::label('file','Select File to Import:',['class'=>'col-md-3']) !!}
                    <div class="col-md-9">
                    {!! Form::file('file', array('class' => 'form-control')) !!}
                    {!! $errors->first('file', '<p class="alert alert-danger">:message</p>') !!}
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            {!! Form::submit('Upload',['class'=>'btn btn-primary']) !!}
            </div>
        </div>
       {!! Form::close() !!}
  </div>
</div>
@endsection
