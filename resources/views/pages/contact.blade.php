@extends('layouts.sideNav')
@section('pageName', 'Overzicht')
@section('titel', 'Overzicht')
@section('content')
  <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
  <script>
    tinymce.init({
      selector: 'textarea',
      menubar: false,  })
  </script>
  <div class="row">
      <div class="col-md-12">
          <h1>Stel hier uw vraag</h1>
          <hr>
          <form action="{{ url('contact') }}" method="POST">
              {{ csrf_field() }}
              <div class="form-group">
                  <label name="email">Email:</label>
                  <input id="email" name="email" class="form-control">
              </div>

              <div class="form-group">
                  <label name="subject">Onderwerp:</label>
                  <input id="subject" name="subject" class="form-control">
              </div>

              <div class="form-group">
                  <label name="message">Vraag of opmerking:</label>
                  <textarea id="message" name="message" class="form-control">Type uw bericht hier...</textarea>
              </div>

              <input type="submit" value="Send Message" class="btn btn-success">
          </form>
      </div>
  </div>

@endsection
