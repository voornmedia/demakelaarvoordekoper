@extends('layouts.sideNav')
@section('pageName', 'Wijzig client')
@section('titel', 'Client wijzigen')
@section('content')

  <script>
$(window).on('load',function(){
  console.log(5 + 6);
//CLICK-HANDLERS=============================
    $('.hoofd-item').click(function(){
        var submenu = $(this).parent().children('.subitem');
        if (submenu.css('display') == 'none') {
            $('.subitem').hide(); //first hide any previously showing submenu's
            submenu.show(); //then show the current submenu
        } else {
            submenu.hide(); //hide the current submenu again
        }
    });
});
  console.log(6 + 6);

  //werkt (nog) niet op een of andere manier
$('#inklap').on('click', function() {
    console.log(5 + 6);
  $('.menu-container').toggleClass('clicked');
  $('.container-inhoud').toggleClass('click');
});

console.log(111);
function confirmDelete() {
var result = confirm('Are you sure you want to delete?');

if (result) {
return true;
} else {
return false;
}
}

  </script>
<div class="row">
    {!! Form::model($client, ['route' => ['client.update', $client->id], 'method' => 'PUT'] ) !!}
  <div class="col-md-8">
    <h1>Wijzig hier de client {{ $client->voornaam}}</h1>
      <p class="lead"> Hieronder de specifieke client met alle invulvelden uit db realtime aanpassen en updaten</p>

      <p>@foreach($client->toArray() as $naam => $inhoud)
        <div @if ($loop->first) class="hidden" @endif>
        {{ form::label($naam, ucfirst($naam ).':')}}
        {{ form::text($naam, null, ["class" => 'form-control']) }}
      </div>
      @endforeach</p>
  </div>

  <div class="col-md-4">
    <div class="well">
      <dl class="dl-horizontal">
        <dt>Client toegevoegd om:</dt>
        <dd>{{ date('j M Y, H:i', strtotime($client->created_at)) }}</dd>
      </dl>
      <dl class="dl-horizontal">
        <dt>Laats upgedate:</dt>
        <dd>{{ date('j M Y, H:i', strtotime($client->updated_at)) }}</dd>
      </dl>
      <hr>
      <div class="row">
        <div class="col-sm-6">
          {!! Html::linkRoute('client.index', 'Cancel', array($client->id), array('class' =>'btn btn-primary btn-block' )) !!}
        </div>
        <div class="col-sm-6">
          {{ form::submit('opslaan', ['class' => 'btn btn-success btn-block'])}}
        </div>
        {!! form::close() !!}
        <div class="col-sm-6">

          {!! form::open(['route' => ['client.destroy', $client->id], 'method' =>'DELETE', 'onsubmit' => 'return confirmDelete()']) !!}
          {{ form::submit('Delete', ['class' => 'btn btn-danger btn-block'])}}
          {!! form::close() !!}
      </div>
    </div>
</div>

</div>

@endsection
