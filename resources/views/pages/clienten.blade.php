@extends('layouts.sideNav')
@section('pageName', 'Clienten')
@section('titel', 'Clienten')
@section('content')
<div class="row">
  <div class="col-md-10">
    <h1>Alle clienten weegeven</h1>
    <p class="lead"> alle Clienten hiero</p>
    <br>
  </div>

  <div class="col-md-2">
    <a href="{{ route('client.create')}}" class="btn btn-block btn-primary btn-h1-spacing">Voeg client toe</a>
  </div>

  <div class="col-md-12">
    <hr>
  </div>

</div>

<div class="row">
  <div class="col-md-12">

    <div class="table-overzicht" id="tabel">
      @include('layouts.tabel')

      <table id="table" class="table table-hover table-bordered table-striped">
        <thead>
          <tr>
            <th>#</th>
            <th>Voorletters</th>
            <th>Voornaam</th>
            <th>Tussen</th>
            <th>achternaam</th>
            <th>Adres</th>
            <th>Huis Nr.</th>
            <th>Plaats</th>
            <th>Email</th>
            <th>Mobiel</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($clienten as $client)
            <tr class='clickable-row' data-href='{{ route('client.edit', ['id' => $client->id]) }}'>
              <th> {{ $client ->id}}</th>
              <td> {{ $client ->voorletters}}</td>
              <td> {{ $client ->voornaam}}</td>
              <td> {{ $client ->tussenvoegsels}}</td>
              <td> {{ $client ->achternaam}}</td>
              <td> {{ $client ->adress}}</td>
              <td> {{ $client ->huisnummer}}</td>
              <td> {{ $client ->woonplaats}}</td>
              <td> {{ $client ->email}}</td>
              <td> {{ $client ->mobiel}}</td>
            </tr>
        @endforeach
        </tbody>
      </table>
      @push('scripts')
        <script>

        $(document).ready(function() {
            var table = $('#table').DataTable();

            $('#table tbody').on('click', 'tr', function () {
                window.location = $(this).data("href");
            } );
        } );
      </script>
      @endpush
    </div>



  </div>
</div>

@endsection
