@extends('layouts.sideNav')
@section('pageName', 'Wijzig pand')
@section('titel', 'Pand informatie bijwerken')
@section('content')

  <script>

console.log(111);
function confirmDelete() {
var result = confirm('Are you sure you want to delete?');

if (result) {
return true;
} else {
return false;
}
}

  </script>
<div class="row">
  {!! Form::model($pand, ['route' => ['pand.update', $pand->id], 'method' => 'PUT'] ) !!}
  <div class="col-md-8">
    <h1>Wijzig hier het pand {{ $pand->straat}}</h1>
      <p class="lead"> Hieronder de specifieke client met alle invulvelden uit db realtime aanpassen en updaten</p>
      <p class="lead">
        @foreach($pand->toArray() as $naam => $inhoud)
          <div @if ($loop->first) class="hidden" @endif>
          {{ form::label($naam, ucfirst($naam ).':')}}
          {{ form::text($naam, null, ["class" => 'form-control']) }}
        </div>
        @endforeach

      </p>
  </div>

  <div class="col-md-4">
    <div class="well">
      <dl class="dl-horizontal">
        <dt>Client toegevoegd om:</dt>
        <dd>{{ date('j F Y, H:i', strtotime($pand->created_at)) }}</dd>
      </dl>
      <dl class="dl-horizontal">
        <dt>Laats upgedate:</dt>
        <dd>{{ date('j F Y, H:i', strtotime($pand->updated_at)) }}</dd>
      </dl>
      <hr>

      <div class="row">
        <div class="col-sm-6">
          {!! Html::linkRoute('pand.index', 'Cancel', array($pand->id), array('class' =>'btn btn-primary btn-block' )) !!}
        </div>
        <div class="col-sm-6">
          {{ form::submit('opslaan', ['class' => 'btn btn-success btn-block'])}}
        </div>
        {!! form::close() !!}
        <div class="col-sm-6">
          {!! Form::open(['route' => ['pand.destroy', $pand->id], 'method' =>'DELETE', 'onsubmit' => 'return confirmDelete()']) !!}
          {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-block'])}}
          {!! Form::close() !!}
        </div>
      </div>

    </div>
</div>

</div>
@endsection
