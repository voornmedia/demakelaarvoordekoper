@extends('layouts.sideNav')

@section('pageName', 'Pand toevoegen')

@section('titel', 'Pand toevoegen')

@section('content')
<div class="row">
  <div class="col-md-6">
    <h1>Nieuwe Pand toevoegen</h1>
      <p>hier komt een form voor het pand</p>

      {!! Form::open(array('route' => 'pand.store', 'data-parsley-validate' => '')) !!}

        {{form::label('straat', 'Straat:')}}
        {{form::text('straat', null, array('class' => 'form-control', 'required' =>'', 'minlength' =>'2', 'maxlength' =>'50'))}}

        {{form::label('huisnummer', 'Huisnummer:')}}
        {{form::text('huisnummer', null, array('class' => 'form-control', 'required' =>'', 'minlength' =>'2', 'maxlength' =>'25'))}}

        {{form::label('toevoeging', 'Toevoeging:')}}
        {{form::text('toevoeging', null, array('class' => 'form-control'))}}

        {{form::label('postcode', 'Postcode:')}}
        {{form::text('postcode', null, array('class' => 'form-control', 'required' =>'', 'minlength' =>'4', 'maxlength' =>'10'))}}

        {{form::label('plaats', 'Plaats:')}}
        {{form::text('plaats', null, array('class' => 'form-control', 'required' =>'', 'minlength' =>'2', 'maxlength' =>'50'))}}

        {{form::label('huidigePrijs', 'Huidige Prijs:')}}
        {{form::text('huidigePrijs', null, array('class' => 'form-control'))}}

        {{form::label('koopConditie', 'Koop conditie:')}}
        {{form::text('koopConditie', null, array('class' => 'form-control'))}}

        {{form::label('bouwjaar', 'Bouwjaar:')}}
        {{form::text('bouwjaar', null, array('class' => 'form-control'))}}

        {{form:: submit('Pand toevoegen', array('class' => 'btn btn-succes btn-lg', 'style' => 'margin-top: 10px;'))}}
      {!! Form::close() !!}

  </div>
  <div class="col-md-6">
    {!! Form::open(array('route' => 'pand.importNew','method'=>'POST','files'=>'true')) !!}
        <div class="row">
           <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    {!! Form::label('file','Select File to Import:',['class'=>'col-md-3']) !!}
                    <div class="col-md-9">
                    {!! Form::file('file', array('class' => 'form-control')) !!}
                    {!! $errors->first('file', '<p class="alert alert-danger">:message</p>') !!}
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            {!! Form::submit('Upload',['class'=>'btn btn-primary']) !!}
            </div>
        </div>
       {!! Form::close() !!}

  </div>
</div>
@endsection
