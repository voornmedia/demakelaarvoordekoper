@extends('layouts.sideNav')
@section('pageName', 'Panden')
@section('titel', 'Panden')
@section('content')
  <div class="row">
    <div class="col-md-10">
      <h1>Alle Panden weegeven</h1>
      <p class="lead"> alle panden hiero</p>
      <br>
    </div>

    <div class="col-md-2">
      <a href="{{ route('pand.create')}}" class="btn btn-block btn-primary btn-h1-spacing">Voeg pand toe</a>
    </div>

    <div class="col-md-12">
      <hr>
    </div>

  </div>

  <div class="row">
    <div class="col-md-12">

      <div class="table-overzicht">
        @include('layouts.tabel')

        <table id="table" class="table table-hover table-bordered table-striped">
          <thead>
            <tr>
          <th>#</th>
          <th>straat</th>
          <th>huisnummer</th>
          <th>postcode</th>
          <th>plaats</th>
          <th>huidige prijs</th>
          <th>koop conditie</th>
          <th>bouwjaar</th>
        </tr>
        </thead>

        <tbody>
          @foreach ($panden as $pand)
            <tr class='clickable-row' data-href='{{ route('pand.edit', ['id' => $pand->id]) }}'>
              <th> {{ $pand ->id}}</th>
              <td> {{ $pand ->straat}}</td>
              <td> {{ $pand ->huisnummer}}</td>
              <td> {{ $pand ->postcode}}</td>
              <td> {{ $pand ->plaats}}</td>
              <td> {{ $pand ->huidigePrijs}}</td>
              <td> {{ $pand ->koopConditie}}</td>
              <td> {{ $pand ->bouwjaar}}</td>
            </tr>
          @endforeach
        </tbody>
      </table>

      @push('scripts')
        <script>

        $(document).ready(function() {
            var table = $('#table').DataTable();

            $('#table tbody').on('click', 'tr', function () {
                window.location = $(this).data("href");
            } );
        } );
      </script>
      @endpush
    </div>

    </div>
  </div>
@endsection
