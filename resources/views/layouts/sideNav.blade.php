@extends('layouts.app')

@section('wrapper')

@push('scripts')
<script>
$(window).on('load',function(){
  console.log(5 + 6);
//CLICK-HANDLERS=============================
    $('.hoofd-item').click(function(){
        var submenu = $(this).parent().children('.subitem');
        if (submenu.css('display') == 'none') {
            $('.subitem').hide(); //haal eerst alle zichtbare uitgeklapte menus weg
            submenu.show(); //laat submenu zien
        } else {
            submenu.hide(); //laat submenu niet meer zien
        }
    });
});
</script>
<script>
$('#inklap').on('click', function() {
  $('.menu-container').toggleClass('clicked');
  $('.container-inhoud').toggleClass('click');
});
</script>
@endpush

<div class="menu-container">
  <div class="item-container">
  <div class="container-item">
    <a href="/"><div class="hoofd-item">
      <img src="{{ asset('img/icons/icon.overview.png') }}" class="icon-hoofdmenu">  Overzicht
    </div></a>
  </div>

  <div class="container-item">
    <div class="hoofd-item">
      <img src="{{ asset('img/icons/icon.gebruikers.png') }}" class="icon-hoofdmenu"> Relaties
    </div>

    <div class="subitem">
      <ul>
        <li><img src="{{ asset('img/icons/icon.profiel.png') }}" class="icon-submenu"><a href="/clienten">Clienten</a></li>
        <li><img src="{{ asset('img/icons/icon.contactpersoon.png') }}" class="icon-submenu"><a href="/clienten">Bedrijven</a></li>
      </ul>
    </div>
  </div>

  <div class="container-item">
    <a href="/panden"><div class="hoofd-item">
      <img src="{{ asset('img/icons/icon.pand.png') }}" class="icon-hoofdmenu"> Panden
    </div></a>

  </div>

  <div class="container-item">
    <div class="hoofd-item">
      <img src="{{ asset('img/icons/icon.taxatie.png') }}" class="icon-hoofdmenu"> Taxaties
    </div>

    <div class="subitem">
      <ul>
        <li><img src="{{ asset('img/icons/icon.stemmers.png') }}" class="icon-submenu"><a href="/taxaties">Taxatierapporten</a></li>
        <li><img src="{{ asset('img/icons/icon.stemmers.png') }}" class="icon-submenu"><a href="/nieuweTaxatie">Nieuwe taxatie</a></li>
        <li><img src="{{ asset('img/icons/icon.domein.png') }}" class="icon-submenu"><a href="/taxaties">Verder taxeren</a></li>
        <li><img src="{{ asset('img/icons/icon.reactie.png') }}" class="icon-submenu"><a href="/taxaties">Rapport archief</a></li>
      </ul>
    </div>
  </div>

  <div class="container-item">
    <div class="hoofd-item">
      <img src="{{ asset('img/icons/icon.mail.png') }}" class="icon-hoofdmenu"> Mailings
    </div>

    <div class="subitem">
      <ul>
        <li><img src="{{ asset('img/icons/icon.domein.png') }}" class="icon-submenu"><a href="/contact">Contact</a></li>
        <li><img src="{{ asset('img/icons/icon.domein.png') }}" class="icon-submenu"><a href="#">Mail template 2</a></li>
        <li><img src="{{ asset('img/icons/icon.domein.png') }}" class="icon-submenu"><a href="#">Mail template 3</a></li>
        <li><img src="{{ asset('img/icons/icon.domein.png') }}" class="icon-submenu"><a href="#">Mail template 4</a></li>
      </ul>
    </div>
  </div>

  <div class="container-item">
    <div class="hoofd-item">
      <img src="{{ asset('img/icons/icon.beheer.png') }}" class="icon-hoofdmenu"> Beheer
    </div>

    <div class="subitem">
      <ul>
        <li><img src="{{ asset('img/icons/icon.teamleden.png') }}" class="icon-submenu"><a href="#">Gebruikers</a></li>

        <li><img src="{{ asset('img/icons/icon.menu.png') }}" class="icon-submenu"><a href="#">Instellingen</a></li>
      </ul>
    </div>
  </div>
</div>

<div class="toggleMenu" id="inklap">
  <img src="{{ asset('img/icons/icon.arrow.png') }}" class="icon-hoofdmenu" id="icon-inklap">
</div>

</div>


<div class="container-inhoud">
<div class="topbalk">
  <img src="{{ asset('img/icons/icon.profiel.png') }}" class="icon-hoofdmenu"> @yield('pageName')
</div>

      <div class="inhoud">
              <div class="panel-body">
                @include('includes._messages')
                @yield('content')
              </div>
      </div>
</div>
@endsection
