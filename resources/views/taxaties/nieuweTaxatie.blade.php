@extends('layouts.sideNav')

@section('pageName', 'Taxatierapport maken')

@section('titel', 'Taxatierapport maken')

<script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>

<script>
  tinymce.init({
    selector: 'textarea',
    menubar: false,  })
</script>

@section('content')
<div class="row">
  <div class="col-md-8">
    <h1>Nieuw taxatierapport maken</h1>
      <p>hier komt een form voor het maken van taxatie rapport afh van het pand met opties enz<br>
      Waarschijnlijk middels een lijst van panden of de optie om een nieuwe toe te voegen via panden handmatig.</p>

      {!! Form::open(array('route' => 'taxatie.store', 'data-parsley-validate' => '', 'files' => true)) !!}

        <div class="row"><div class="col-md-6">
        {{ form::label('clienten_id', 'Client:')}}
        <select class="form-control" name="clienten_id">
          @foreach($clienten as $client)
            <option value="{{$client->id}}">{{$client->voornaam}}</option>
          @endforeach
        </select>
      </div>
      <div class="col-md-6">
        {{ form::label('pand_id', 'Pand:')}}
        <select class="form-control" name="pand_id">
          @foreach($panden as $pand)
            <option value="{{$pand->id}}">{{$pand->straat}}</option>
          @endforeach
        </select>
      </div></div>
        <hr>
        <div class="row">
        <div class="col-md-6">
        {{ form::label('soort_woning', 'Soort:')}}
        <select class="form-control" name="soort_woning">
            <option value="vrijstaande">vrijstaande</option>
            <option value="halfvrijstaande">halfvrijstaande</option>
            <option value="tussengelegen">tussengelegen</option>
            <option value="hoekwoning">hoekwoning</option>
            <option value="appartementsrecht">appartementsrecht</option>
            <option value="galerijflat">galerijflat</option>
            <option value="penthouse">penthouse</option>
            <option value="flatwoning">flatwoning</option>
        </select>
      </div>
<div class="col-md-6">
        {{ form::label('type_woning', 'Type:')}}
        <select class="form-control" name="type_woning">
            <option value="woning">woning</option>
            <option value="villa">villa</option>
            <option value="stadswoning">stadswoning</option>
            <option value="boerderijwoning">boerderijwoning</option>
            <option value="carréboerderij">carréboerderij</option>
        </select>
        </div>
      </div><br>

<div class="row">
        <div class="col-md-4">
        {{ form::label('uitbouw', 'Uitbouw:')}}
        {{ Form::checkbox('omschrijving[]', 'uitbouw', false) }}
        <br>
        {{ form::label('cv', 'Cv:')}}
        {{ Form::checkbox('omschrijving[]', 'cv', true) }}
        <br>
        {{ form::label('berging', 'Berging:')}}
        {{ Form::checkbox('omschrijving[]', 'berging', false) }}
      </div>

      <div class="col-md-4">
      {{ form::label('garage', 'Garage:')}}
      {{ Form::checkbox('omschrijving[]', 'garage', false) }}
      <br>
      {{ form::label('zwembad', 'Zwembad:')}}
      {{ Form::checkbox('omschrijving[]', 'zwembad', false) }}
      <br>
      {{ form::label('schuur', 'Schuur:')}}
      {{ Form::checkbox('omschrijving[]', 'schuur', false) }}
    </div>
    <div class="col-md-4">
    {{ form::label('carport', 'Carport:')}}
    {{ Form::checkbox('omschrijving[]', 'carport', false) }}
    <br>
    {{ form::label('tuin', 'Tuin:')}}
    {{ Form::checkbox('omschrijving[]', 'tuin', true) }}
    <br>
    {{ form::label('invul', 'Invul:')}}
    {{ Form::checkbox('omschrijving[]', 'invul', false) }}
  </div>
</div>

        <hr>
        {{ form::label('hyp4', 'Eigendomsakte:')}}
        <select class="form-control" name="hyp4">
            <option value="1">Beide eigenaar</option>
            <option value="2">Één eigenaar</option>
            <option value="3">Beide eigenaar, een overleden</option>
            <option value="4">Erfgenamen eigenaar</option>
            <option value="5">Gevolmachtigde RechtB</option>
            <option value="6">Cöop Bouwvereniging</option>
            <option value="7">Sitchting, BV of lichaam</option>
        </select>
        <hr>
        {{ form::label('featured_image', 'Upload afbeeling hier')}}
        {{ form::file('featured_image')}}

        {{form:: submit('Taxatie rapport aanmaken', array('class' => 'btn btn-success btn-lg', 'style' => 'margin-top: 10px;'))}}
      {!! Form::close() !!}
  </div>
</div>
@endsection
