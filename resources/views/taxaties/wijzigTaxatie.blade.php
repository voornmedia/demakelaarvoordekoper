@extends('layouts.sideNav')
@section('pageName', 'Wijzig taxatie')
@section('titel', 'Taxatie informatie bijwerken')
@section('content')
<div class="row">
  {!! Form::model($taxatie, ['route' => ['taxatie.update', $taxatie->id], 'method' => 'PUT'] ) !!}
  <div class="col-md-8">
    <h1>Wijzig hier het taxatierapport: {{ $taxatie->titel}}</h1>
      <p class="lead"> Nogmaals het form met alle vragen voor de taxatie die tekst update</p><br>
      <p class="lead">
        {{ form::label('clienten_id', 'Client:') }}
        {{ form::select('clienten_id', $clients, null, ["class" => 'form-control'] ) }}

        {{ form::label('pand_id', 'Pand:')}}
        {{ form::select('pand_id', $pands, null, ["class" => 'form-control'] ) }}

        {{ form::label('soort_woning', 'Soort:')}}
        <select class="form-control" name="soort_woning">
            <option value="vrijstaande" @if ($taxatie->soort_woning == "vrijstaande") selected="selected" @endif>vrijstaande</option>
            <option value="halfvrijstaande" @if ($taxatie->soort_woning == "halfvrijstaande") selected="selected" @endif>halfvrijstaande</option>
            <option value="tussengelegen" @if ($taxatie->soort_woning == "tussengelegen") selected="selected" @endif>tussengelegen</option>
            <option value="hoekwoning" @if ($taxatie->soort_woning == "hoekwoning") selected="selected" @endif>hoekwoning</option>
            <option value="appartementsrecht" @if ($taxatie->soort_woning == "appartementsrecht") selected="selected" @endif>appartementsrecht</option>
            <option value="galerijflat" @if ($taxatie->soort_woning == "galerijflat") selected="selected" @endif>galerijflat</option>
            <option value="penthouse" @if ($taxatie->soort_woning == "penthouse") selected="selected" @endif>penthouse</option>
            <option value="flatwoning" @if ($taxatie->soort_woning == "flatwoning") selected="selected" @endif>flatwoning</option>
        </select>

        {{ form::label('type_woning', 'Type:')}}
        <select class="form-control" name="type_woning">
            <option value="woning" @if ($taxatie->type_woning == "woning") selected="selected" @endif>woning</option>
            <option value="villa" @if ($taxatie->type_woning == "villa") selected="selected" @endif>villa</option>
            <option value="stadswoning" @if ($taxatie->type_woning == "stadswoning") selected="selected" @endif>stadswoning</option>
            <option value="boerderijwoning" @if ($taxatie->type_woning == "boerderijwoning") selected="selected" @endif>boerderijwoning</option>
            <option value="carréboerderij" @if ($taxatie->type_woning == "carréboerderij") selected="selected" @endif>carréboerderij</option>
        </select>

        {{ form::label('hyp4', 'Eigendomsakte:')}}
        <select class="form-control" name="hyp4">
            <option value="1" @if ($taxatie->hyp4 == "1") selected="selected" @endif>Beide eigenaar</option>
            <option value="2" @if ($taxatie->hyp4 == "2") selected="selected" @endif>Één eigenaar</option>
            <option value="3" @if ($taxatie->hyp4 == "3") selected="selected" @endif>Beide eigenaar, een overleden</option>
            <option value="4" @if ($taxatie->hyp4 == "4") selected="selected" @endif>Erfgenamen eigenaar</option>
            <option value="5" @if ($taxatie->hyp4 == "5") selected="selected" @endif>Gevolmachtigde RechtB</option>
            <option value="6" @if ($taxatie->hyp4 == "6") selected="selected" @endif>Cöop Bouwvereniging</option>
            <option value="7" @if ($taxatie->hyp4 == "7") selected="selected" @endif>Sitchting, BV of lichaam</option>
        </select>
      </p>
  </div>

  <div class="col-md-4">
    <div class="well">
      <dl class="dl-horizontal">
        <dt>Client toegevoegd om:</dt>
        <dd>{{ date('j M Y, H:i', strtotime($taxatie->created_at)) }}</dd>
      </dl>
      <dl class="dl-horizontal">
        <dt>Laats upgedate:</dt>
        <dd>{{ date('j M Y, H:i', strtotime($taxatie->updated_at)) }}</dd>
      </dl>
      <hr>
      <div class="row">
        <div class="col-sm-6">
          {!! Html::linkRoute('taxatie.show', 'Cancel', array($taxatie->id), array('class' =>'btn btn-danger btn-block' )) !!}
        </div>
        <div class="col-sm-6">
          {{ form::submit('opslaan', ['class' => 'btn btn-success btn-block'])}}
        </div>
      </div>
    </div>
</div>
{!! form::close() !!}
</div>
@endsection
