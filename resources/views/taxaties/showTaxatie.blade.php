@extends('layouts.sideNav')
@section('pageName', 'Show taxatie')
@section('titel', 'Taxatie informatie laten zien en printen')
@section('content')
@php
  $beideEigenaar = "Beide in het kadastraal uittreksel object genoemde personen zijn, ieder voor de onverdeelde helft, eigenaar van het onderhavige object, te weten de grond met opstallen.";
  $eenEigenaar = 'De in het kadastraal uittreksel object genoemde persoon is alleen eigenaar van het onderhavige object, te weten de grond met opstallen. Indien hij/zij een duurzamen huishouding met iemand voert, dient er bij een voorgenomen verkoop, op basis van het Burgerlijk Wetboek, toestemming te worden gegeven door degene waarmee die huishouding wordt gevoerd. Ook als die persoon niet tot de onroerende zaak bevoegd is. Dit omdat het dat het dan gaat om de gezamenlijk bewoonde woning.';
  $eenOverleden = "Beide in het kadastraal uittreksel genoemde personen zijn, ieder voor de onverdeelde helft, eigenaar van het onderhavige object, te weten de grond met opstallen. Een van hen is overleden zodat de ander, alleen of samen met mogelijke erfgenamen (kinderen) bevoegd is tot de eigendom. Omtrend deze bevoegdheid dient de verkopende partij nadere informatie en/of bewijs te verstrekken. (verklaring van erfrecht/testament/volmacht/e.d.)";
  $erfgenaam = "De in het kadastraal uitreksel object genoemde perso(o)n(en) waren eigenaar van het onderhavige object, te weten de grond met opstallen. Door hun overlijden zijn er erfgenamen in hun rechten getreden en derhalve bevoegd. Omtrend de bevoegdheid dient de verkopende partij nadere informatie en/of bewijs te verstrekken. (verklaring van erfrecht/testament/volmacht/e.d.)";
  $gevolmachtigd = "Omdat de eigena(a)r(en) niet meer handelingsbekwaam werden geacht is, middels een rechtelijk bevel, de beschikkingsbevoegdheid over de eigendom van de onroerende zaak bij een gevolmatigde komen te liggen. Hij handelt in deze niet voor zichzelf doch voor de oorspronkelijke eigena(a)r(en). Bij de verkoop van de onroerende zaak dient er, ter bescherming van de rechten van de rechthebbbende, alsnog een rechtelijke goedkeuring te komen. Hierbij toets de rechter de verkoopopbrengts aan een (van te voren) opgesteld taxatierapport van een onafhanelijk taxateur. Eerst na rechtelijke goedkeuring wordt de koopovereenkomst definitief.";
  $coop = "Het eigendomsrecht is hierbij in tweeën geknipt in, enerzijds een blote eigendom en anderzijds een zakelijk gerechtigde. De in het kadastraal uittreksel object genoemde Coöperatieve Bouwvereniging is hierbij de zichtbare blote eigenaar.  Deze blote eigenaar heeft geen rechten tot de onroerende zaak. Hij kan ze niet verkopen of verhuren, noch bewonen, met hypotheek of andere schuld belasten, etc. De bewoner is in deze de zakelijk gerechtigde.  Deze niet zichtbare zakelijk gerechtigde heeft alle rechten zoals die van bewoning, verhuur, verkoop, hypotheek, etc. Alleen hij is (samen met de blote eigenaar) beschikkingsbevoegd. Deze inmiddels in onbruik geraakte constructie werd in de vorige eeuw gebezigd om betaling van de overdrachtsbelasting te omzeilen. Het lek in de Wet is inmiddels gedicht waardoor de Coöperaties langzaam uitsterven.";
  $stichting = "De in het kadastraal uitreksel object genoemde rechtpersoon is eigenaar van het onderhavige object, te weten de grond met opstallen."
@endphp

  <script>
$(window).on('load',function(){
  console.log(5 + 6);
//CLICK-HANDLERS=============================
    $('.hoofd-item').click(function(){
        var submenu = $(this).parent().children('.subitem');
        if (submenu.css('display') == 'none') {
            $('.subitem').hide(); //first hide any previously showing submenu's
            submenu.show(); //then show the current submenu
        } else {
            submenu.hide(); //hide the current submenu again
        }
    });
});
  console.log(6 + 6);

  //werkt (nog) niet op een of andere manier
$('#inklap').on('click', function() {
    console.log(5 + 6);
  $('.menu-container').toggleClass('clicked');
  $('.container-inhoud').toggleClass('click');
});

console.log(111);
function confirmDelete() {
var result = confirm('Are you sure you want to delete?');

if (result) {
return true;
} else {
return false;
}
}

  </script>
<div class="row">
  <div class="col-md-8">
    <h1>{{ $taxatie->titel}}</h1>
      <p class="lead">
      Dit rapport is opgemaakt op <b>{{ date('j M Y', strtotime($taxatie->created_at)) }}</b> in opdrachht van
      <b>{{ $taxatie->clienten['titel']}}{{ $taxatie->clienten['voorletters']}} {{ $taxatie->clienten['achternaam']}}</b>.
     Het betreft <b>{{ $taxatie->soort_woning}} {{ $taxatie->type_woning}}</b> met <b>{{ $taxatie->woning_omschrijving}}</b> gelegen
     aan de <b>{{ $taxatie->pand['straat']}} {{ $taxatie->pand['huisnummer']}}</b> in <b>{{ $taxatie->pand['plaats']}}</b>
     ."</p>

     <p class="lead">
       @if ($taxatie->hyp4 == 1)
          {{$beideEigenaar}}
       @elseif ($taxatie->hyp4 == 2)
          {{$eenEigenaar}}
       @elseif ($taxatie->hyp4 == 3)
          {{$eenOverleden}}
       @elseif ($taxatie->hyp4 == 4)
         {{$erfgenaam}}
       @elseif ($taxatie->hyp4 == 5)
         {{$gevolmachtigd}}
       @elseif ($taxatie->hyp4 == 6)
         {{$coop}}
       @elseif ($taxatie->hyp4 == 7)
         {{$stichting}}
      @else
        Er is geen geldigde hyp deel 4 optie ingegeven
      @endif
     </p>

     <img src="{{ asset('img/taxaties/'. $taxatie->afbeelding) }}" height="400" width="600"/>
      <hr>
  </div>

  <div class="col-md-4">
    <div class="well">
      <dl class="dl-horizontal">
        <dt>Taxatie toegevoegd om:</dt>
        <dd>{{ date('j M Y, H:i', strtotime($taxatie->created_at)) }}</dd>
      </dl>
      <dl class="dl-horizontal">
        <dt>Laats upgedate:</dt>
        <dd>{{ date('j M Y, H:i', strtotime($taxatie->updated_at)) }}</dd>
      </dl>
      <dl class="dl-horizontal">
        <dt>Client:</dt>
        <dd>{{ $taxatie->clienten['voornaam']}}</dd>
      </dl>
      <dl class="dl-horizontal">
        <dt>Pand:</dt>
        <dd>{{ $taxatie->pand['straat']}}</dd>
      </dl>
      <hr>
      <div class="row">
        <div class="col-sm-6">
          {!! Html::linkRoute('taxatie.edit', 'Edit', array($taxatie->id), array('class' =>'btn btn-success btn-block' )) !!}
        </div>
        <div class="col-sm-6">
          {!! Form::open(['route' => ['taxatie.destroy', $taxatie->id], 'method' =>'DELETE', 'onsubmit' => 'return confirmDelete()']) !!}
          {{ Form::submit('Delete', ['class' => 'btn btn-danger btn-block'])}}
          {!! Form::close() !!}
        </div><br><br>
        <div class="col-sm-6">
          {!! Html::linkRoute('taxatie.index', 'Terug', array($taxatie->id), array('class' =>'btn btn-primary btn-block' )) !!}
        </div>
        <div class="col-sm-6">
          <a href="{{Route('taxatie.pdf', $taxatie->id)}}" target="_blank"><button class="btn btn-primary btn-block">Export PDF</button></a>
        </div>
      </div>
    </div>
</div>
@endsection
