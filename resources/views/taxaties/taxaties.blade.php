@extends('layouts.sideNav')
@section('pageName', 'Taxaties')
@section('titel', 'Taxaties')
@section('content')
  <div class="row">
    <div class="col-md-10">
      <h1>Alle taxatierapporten</h1>
      <p class="lead"> alle rapporten in tabel weergeven</p>
      <br>
    </div>

    <div class="col-md-2">
      <a href="{{ route('taxatie.create')}}" class="btn btn-block btn-primary btn-h1-spacing">Voeg taxatie toe</a>
    </div>

    <div class="col-md-12">
      <hr>
    </div>

  </div>

  <div class="row">
    <div class="col-md-12">

      <div class="table-overzicht">
        @include('layouts.tabel')

        <table id="table" class="table table-hover table-bordered table-striped">
          <thead>
            <tr>
          <th>#</th>
          <th>Straat</th>
          <th>Client</th>
          <th>Woning type</th>
          <th>Aangemaakt</th>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody>
          @foreach ($taxaties as $taxatie)
            <tr class='clickable-row' data-href='{{ route('taxatie.show', ['id' => $taxatie->id]) }}'>
              <th> {{ $taxatie ->id}}</th>
              <td> {{ $taxatie->pand['straat'] }} {{$taxatie->pand['huisnummer']}} {{$taxatie->pand['plaats']}}</td>
              <td> {{ $taxatie->clienten['voornaam'] }} {{ $taxatie->clienten['achternaam'] }}</td>
              <th> {{ $taxatie ->soort_woning}} {{ $taxatie ->type_woning}}</th>
              <td> {{ date('j M Y, H:i', strtotime($taxatie->created_at)) }}</td>
              <td> <a href=" {{ route('taxatie.show', $taxatie->id)}}" class="btn btn-default">show</a></td>
              <td> <a href=" {{ route('taxatie.edit', $taxatie->id)}}" class="btn btn-default">edit</a></td>
            </tr>
          @endforeach
        </tbody>
      </table>
      @push('scripts')
        <script>

        $(document).ready(function() {
            var table = $('#table').DataTable();

            $('#table tbody').on('click', 'tr', function () {
                window.location = $(this).data("href");
            } );
        } );
      </script>
      @endpush
    </div>

    </div>
  </div>
@endsection
