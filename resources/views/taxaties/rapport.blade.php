<style>
@page { margin: 0px; }
.content {margin: 50px;}
.page-break {page-break-after: always;}
.header {width: 101%; height:150px; background-color:orange; top:-100px;left:-50px;}
.row {display:inline-block;}
.center { margin-left: 20%;}
.afbeelding {height: 400px; width:600px; margin-left: 10%; background-color: #f7f7f7;}
.logo { float:right; padding: 15px; margin-right: 20px;}
h1 {style:none; margin-top:0px; position: absolute; color:white;padding-top:50px;padding-left:40px;font-size: 34;}
ul { list-style-image: url({{ public_path('/img/logowhite.png') }});}
</style>

@php
  $beideEigenaar = "Beide in het kadastraal uittreksel object genoemde personen zijn, ieder voor de onverdeelde helft, eigenaar van het onderhavige object, te weten de grond met opstallen.";
  $eenEigenaar = 'De in het kadastraal uittreksel object genoemde persoon is alleen eigenaar van het onderhavige object, te weten de grond met opstallen. Indien hij/zij een duurzamen huishouding met iemand voert, dient er bij een voorgenomen verkoop, op basis van het Burgerlijk Wetboek, toestemming te worden gegeven door degene waarmee die huishouding wordt gevoerd. Ook als die persoon niet tot de onroerende zaak bevoegd is. Dit omdat het dat het dan gaat om de gezamenlijk bewoonde woning.';
  $eenOverleden = "Beide in het kadastraal uittreksel genoemde personen zijn, ieder voor de onverdeelde helft, eigenaar van het onderhavige object, te weten de grond met opstallen. Een van hen is overleden zodat de ander, alleen of samen met mogelijke erfgenamen (kinderen) bevoegd is tot de eigendom. Omtrend deze bevoegdheid dient de verkopende partij nadere informatie en/of bewijs te verstrekken. (verklaring van erfrecht/testament/volmacht/e.d.)";
  $erfgenaam = "De in het kadastraal uitreksel object genoemde perso(o)n(en) waren eigenaar van het onderhavige object, te weten de grond met opstallen. Door hun overlijden zijn er erfgenamen in hun rechten getreden en derhalve bevoegd. Omtrend de bevoegdheid dient de verkopende partij nadere informatie en/of bewijs te verstrekken. (verklaring van erfrecht/testament/volmacht/e.d.)";
  $gevolmachtigd = "Omdat de eigena(a)r(en) niet meer handelingsbekwaam werden geacht is, middels een rechtelijk bevel, de beschikkingsbevoegdheid over de eigendom van de onroerende zaak bij een gevolmatigde komen te liggen. Hij handelt in deze niet voor zichzelf doch voor de oorspronkelijke eigena(a)r(en). Bij de verkoop van de onroerende zaak dient er, ter bescherming van de rechten van de rechthebbbende, alsnog een rechtelijke goedkeuring te komen. Hierbij toets de rechter de verkoopopbrengts aan een (van te voren) opgesteld taxatierapport van een onafhanelijk taxateur. Eerst na rechtelijke goedkeuring wordt de koopovereenkomst definitief.";
  $coop = "Het eigendomsrecht is hierbij in tweeën geknipt in, enerzijds een blote eigendom en anderzijds een zakelijk gerechtigde. De in het kadastraal uittreksel object genoemde Coöperatieve Bouwvereniging is hierbij de zichtbare blote eigenaar.  Deze blote eigenaar heeft geen rechten tot de onroerende zaak. Hij kan ze niet verkopen of verhuren, noch bewonen, met hypotheek of andere schuld belasten, etc. De bewoner is in deze de zakelijk gerechtigde.  Deze niet zichtbare zakelijk gerechtigde heeft alle rechten zoals die van bewoning, verhuur, verkoop, hypotheek, etc. Alleen hij is (samen met de blote eigenaar) beschikkingsbevoegd. Deze inmiddels in onbruik geraakte constructie werd in de vorige eeuw gebezigd om betaling van de overdrachtsbelasting te omzeilen. Het lek in de Wet is inmiddels gedicht waardoor de Coöperaties langzaam uitsterven.";
  $stichting = "De in het kadastraal uitreksel object genoemde rechtpersoon is eigenaar van het onderhavige object, te weten de grond met opstallen.";

$datum = date("j F Y");
$dagvanweek = date("l");
$arraydag = array("Zondag","Maandag","Dinsdag","Woensdag","Donderdag","Vrijdag","Zaterdag");
$dagvanweek = $arraydag[date("w")];
$arraymaand = array("Januari","Februari","Maart","April","Mei","Juni","Juli","Augustus","September","Oktober","November","December");
$datum = date("j ") . $arraymaand
[date("n") - 1] . date(" Y");

$root= '$_SERVER["DOCUMENT_ROOT"]';
@endphp

<div class="header"><h1>Aankooptaxatie</h1><img class="logo" src="{{ public_path('/img/logowhite.png') }}"/></div><br>
<div class="content">
  <div class="center"><h2>{{ $taxatie->pand['straat']}} {{ $taxatie->pand['huisnummer']}}, {{ $taxatie->pand['postcode']}} {{ $taxatie->pand['plaats']}}</h2></div>
  <img class="afbeelding" src="{{ public_path('/img/taxaties/'. $taxatie->afbeelding) }}" height="400" width="600"/><br>
  </div><br><br><br><br><br><br><br><br><br><br><hr><br>
  <div class="center"> In opdracht van {{ $taxatie->clienten['titel']}} {{ $taxatie->clienten['voorletters']}} {{ $taxatie->clienten['achternaam']}} uitgevoerd op @php echo "d.d. $dagvanweek, $datum";   @endphp</div>

</div>


 <div class="page-break"></div>
 <div class="header"><h1>Inhoudsopgave</h1><img class="logo" src="{{ public_path('/img/logowhite.png') }}"/></div><br>
 <div class="content">
 <h1>Inhoud</h1>
<ul>
<li>1.	Algemeen	3
<li>1.1	De makelaar:	3
<li>1.2	De opdracht:	3
<li>1.3	Het doel:	3
<li>2.	De juridische kaders	4
<li>2.1 Het kadastraal uittreksel object hypotheek 4	4
<li>2.2 De aankomsttitel	6
<li>2.3 Het uittreksel hypotheek 3	8
<li>2.5 De bestemming	8
<li>2.6 De bescherming als Monument	9
<li>2.7 De bodemkwaliteit	9
<li>2.8 Olietank	10
<li>2.9 De Onderaards gangen	10
<li>3.	De intrinsieke kaders:	12
<li>3.1 De ligging	12
<li>3.2 Bouw en constructie	12
<li>3.3 Asbest	13
<li>3.4 De isolatie	14
<li>3.5 De afwerking	14
<li>4.	Onderhoud en gebreken	16
<li>4.1 Het onderhoud en de gebreken	16
<li>4.2 De kosten	26
<li>4.3 De kosten alternatief	26
<li>4.3 De afmetingen	28
<li>5.	De Marktpositie	29
<li>6.	Taxatiekaders:	31
<li>7.	De waarde	32
</div>
<div class="page-break"></div>

<!--pagina 2-->
<div class="header"><h1>1. Algemeen</h1><img class="logo" src="{{ public_path('/img/logowhite.png') }}"/></div><br>
<div class="content">
<h4>BEEK, @php echo "d.d. $dagvanweek, $datum";   @endphp<h4>
<br>

<p><b>1.1	 De makelaar:</b><br>
De heer <b>J.M.E. (Hans) Voorn</b>, beëdigd makelaar in onroerend goed, lid NVM en als gecertificeerd makelaar ingeschreven
in het vastgoedregisters van:<br>
•	Vastgoedcert R´dam kamer Wonen/MKB RMT07.121.2321<br>
Kantoorhoudende aan de <b>Julianalaan 22, 6191 AM BEEK</b>.</p>

<p><b>1.2	 De opdracht:</b><br>
In opdracht van <b>{{ $taxatie->clienten['titel']}} {{ $taxatie->clienten['voorletters']}} {{ $taxatie->clienten['achternaam']}}</b><br>
heeft er op <b>{{ date('j M Y', strtotime($taxatie->created_at)) }}</b> een opname plaatsgevonden van:<br><br>

Het betreft een <b>{{ $taxatie->soort_woning}} {{ $taxatie->type_woning}}</b> met <b>{{ $taxatie->woning_omschrijving}}</b> gelegen
aan de <br><b>{{ $taxatie->pand['straat']}} {{ $taxatie->pand['huisnummer']}}</b> in <b>{{ $taxatie->pand['plaats']}}</b>.<br><br>

<b>Kadastraal bekend als:</b><br>
Gemeente 	: «Gemeente»<br>
Sectie 	: «Sectie»<br>
nummer 	: «NummerAindex»<br>
groot 	: «Grootte_in_m» m²<br>
<!--«voorGem1» 	: «Gemeente1» «voorSec1» 	: «Sectie1» «voorNum1» 	: «NummerAindex1» «voorGroot1» 	: «Grootte_in_m1»
«voorGem2» 	: «Gemeente2» «voorSec2» 	: «Sectie2» «voorNum2» 	: «NummerAindex2» «voorGroot2» 	: «Grootte_in_m2»
			«Totaal_m»--></p>

<p><b>1.3	 Het doel:</b><br>
Het doel van de opname en dit uitgewerkt rapport is het verkrijgen van inzicht in het object, de juridische status, omgevingsfactoren,
 zijn intrinsieke waarden, de bouwtechnische aspecten, de onderhoudskosten en de marktwaarde van het totaal.
 Een en ander in verband met een, door opdrachtgever, te nemen beslissing betreffende een mogelijke aankoop.
</p>
</div>
<div class="page-break"></div>
<!--pagina 3-->
<div class="header"><h1>2.	DE JURIDISCHE KADERS</h1><img class="logo" src="{{ public_path('/img/logowhite.png') }}"/></div><br>
<div class="content">
<p><b>2.1 Het kadastraal uittreksel object hypotheek 4</b><br>
Uit het bijgevoegd uittreksel gaat naar voren dat:<br>
@if ($taxatie->hyp4 == 1)
   {{$beideEigenaar}}
@elseif ($taxatie->hyp4 == 2)
   {{$eenEigenaar}}
@elseif ($taxatie->hyp4 == 3)
   {{$eenOverleden}}
@elseif ($taxatie->hyp4 == 4)
  {{$erfgenaam}}
@elseif ($taxatie->hyp4 == 5)
  {{$gevolmachtigd}}
@elseif ($taxatie->hyp4 == 6)
  {{$coop}}
@elseif ($taxatie->hyp4 == 7)
  {{$stichting}}
@else
 Er is geen geldigde hyp deel 4 optie ingegeven
@endif
</p>

<p><b>2.2 De aankomsttitel</b><br>
⌂ «Aankomsttitel»</p>

<p><b>2.3 De beperkingen van de eigendom</b><br>
⌂ «belemmeringen_alg»</p>

<p>«Bijz_erfdienst_tekst»<br>
⌂ «Bijz_erfdienst_opsom»</p>

<p>«ketting_tekst»<br>
⌂ «Ketting_opsom»</p>

<p>«Kwal_verpl_tekst»<br>
⌂	«Kwal_verpl_opsom»</p>

<p>«kwal_eind_tekst»</p>

<p><b>2.4 Het uittreksel hypotheek 3</b><br>
Uit het bijgevoegd kadastraal uittreksel hypotheken deel 3, blijkt dat het register goed «hyp_3»</p>


<p><b>2.5 De bestemming</b><br>
In het vigerende bestemmingsplan wordt het registergoed aangeduid bestemd te zijn om te gebruiken voor:<br>
⌂	«BP»</p>

<p>«bp_bezwaar»<br>
Indien en voor zover de beoogde bestemming door opdrachtgever afwijkt van de geldende bestemming dan is nader onderzoek bij c.q. overleg met de betreffende gemeente noodzakelijk.</p>
</div>

<div class="page-break"></div>
<!--pagina 4-->
<div class="header"></div>
<div class="content">

 <h1>hypotheken deel 4</h1>
 <p class="lead">
   @if ($taxatie->hyp4 == 1)
      {{$beideEigenaar}}
   @elseif ($taxatie->hyp4 == 2)
      {{$eenEigenaar}}
   @elseif ($taxatie->hyp4 == 3)
      {{$eenOverleden}}
   @elseif ($taxatie->hyp4 == 4)
     {{$erfgenaam}}
   @elseif ($taxatie->hyp4 == 5)
     {{$gevolmachtigd}}
   @elseif ($taxatie->hyp4 == 6)
     {{$coop}}
   @elseif ($taxatie->hyp4 == 7)
     {{$stichting}}
  @else
    Er is geen geldigde hyp deel 4 optie ingegeven
  @endif
 </p>
 </div>
 <div class="page-break"></div>
 <div class="header"></div>
 <div class="content">
 </div>
 pagina 7



 <div class="page-break"></div>
 <div class="header"></div>
 <div class="content">
 </div>
 pagina 8



 <div class="page-break"></div>
 <div class="header"></div>
 <div class="content">
 </div>
pagina 9


 <div class="page-break"></div>
 <div class="header"></div>
 <div class="content">
 </div>
