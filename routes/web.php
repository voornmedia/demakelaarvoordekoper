<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// authetication RouteS
//Route::get('registreer', 'PagesController@getRegister');
//Route::get('login', 'PagesController@getLogin');

Route::get('clienten', 'ClientController@Index' );
Route::get('nieuweClient', 'ClientController@create' );

Route::get('panden', 'PandController@Index' );
Route::get('nieuwePand', 'PandController@create' );
Route::get('test', 'PandController@Test' );

Route::get('taxaties', 'TaxatieController@Index' );
Route::get('nieuweTaxatie', 'TaxatieController@create' );

Route::get('inloggen', 'PagesController@getInloggen' );

Route::get('/', 'PagesController@getIndex' );

Route::get('contact', 'PagesController@getContact');
Route::post('contact', 'PagesController@postContact');

Route::resource('client', 'ClientController');
Route::resource('pand', 'PandController');
Route::resource('taxatie', 'TaxatieController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/taxatie.pdf/{id}',  'taxatieController@pdf')->name('taxatie.pdf');

//excel import en export routes van excel maatsites
//Route::get('import-export-csv-excel',array('as'=>'excel.import','uses'=>'FileController@importExportExcelORCSV'));
//Route::post('import-csv-excel',array('as'=>'import-csv-excel','uses'=>'FileController@importFileIntoDB'));
//Route::get('download-excel-file/{type}', array('as'=>'excel-file','uses'=>'FileController@downloadExcelFile'));
Route::post('importNew', 'PandController@pandImportNew')->name('pand.importNew');
Route::post('importNewClient', 'ClientController@ClientImportNewClient')->name('client.importNewClient');
